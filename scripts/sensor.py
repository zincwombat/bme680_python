# !/usr/bin/env python3
import bme680
import time
import json
import os
import logging
import sys
from datetime import datetime
from datetime import timezone
import paho.mqtt.client as mqtt

DEBUG_ENABLED = os.getenv('DEBUG', False)
HOST = os.getenv('HOST','nohost')
MQTT_HOST = os.getenv('MQTT_HOST')
MQTT_PORT = os.getenv('MQTT_PORT', 1883)
MQTT_KEEPALIVE_INTERVAL = os.getenv('MQTT_KEEPALIVE_INTERVAL', 60)
MQTT_TOPIC = os.getenv('MQTT_TOPIC')
MQTT_CLIENT_ID = os.getenv('MQTT_CLIENT_ID')
SENSOR_ADDRESS = os.getenv('SENSOR_ADDRESS', 0x76)


def init_logging():
    if DEBUG_ENABLED is True:
        logging_level = logging.DEBUG
    else:
        logging_level = logging.INFO
    logging.basicConfig(format='%(asctime)s - %(levelname)s %(message)s', datefmt='%Y%m%d %I:%M:%S %p',
                        level=logging_level)
    if DEBUG_ENABLED is True:
        logging.warning("DEBUG enabled")


def init_mqtt():
    if MQTT_HOST is None:
        logging.critical("MQTT Host is not defined, exiting")
        exit(1)
    elif MQTT_TOPIC is None:
        logging.critical("MQTT Topic is not defined, exiting")
        exit(1)
    elif MQTT_CLIENT_ID is None:
        logging.critical("MQTT Client ID is not defined, exiting")
        exit(1)
    else:
        logging.info("Using MQTT_HOST: %s MQTT_PORT: %s Topic: %s", MQTT_HOST, MQTT_PORT, MQTT_TOPIC)
        logging.info("MQTT Client ID: %s", MQTT_CLIENT_ID)

    mqtt.Client.client_connected = False


def on_publish(client, userdata, mid):
    logging.debug("Message published to MQTT, message id: %s", mid)


def on_connect(client, userdata, flags, rc):
    logging.info("Connection returned result: %s", mqtt.connack_string(rc))
    mqtt.Client.client_connected = True


def on_disconnect(client, userdata, rc):
    logging.info("disconnecting reason: %s", str(rc))
    if rc != 0:
        logging.warning("Unexpected disconnection reason: %s", str(rc))


def on_log(client, obj, level, string):
    if DEBUG_ENABLED is True:
        logging.debug(string)


def init_sensor():
    global sensor, e
    # NOTE: the gravity bcm680 module from dfrobot uses the i2c address 0x77,
    # the pimoroni module uses 0x76
    try:
        sensor = bme680.BME680(SENSOR_ADDRESS)
    except Exception as e:
        logging.critical('Failed to initialise sensor {%s}', e)
        sys.exit()
    # These oversampling settings can be tweaked to
    # change the balance between accuracy and noise in
    # the data.
    sensor.set_humidity_oversample(bme680.OS_2X)
    sensor.set_pressure_oversample(bme680.OS_4X)
    sensor.set_temperature_oversample(bme680.OS_8X)
    sensor.set_filter(bme680.FILTER_SIZE_3)


def read_sensor():
    s_temp = 0
    s_pres = 0
    s_hum = 0
    s_cnt = 0

    # take this many samples and calculate the average

    samples = 10

    for i in range(0, samples):
        if sensor.get_sensor_data():
            s_cnt += 1
            s_temp += sensor.data.temperature
            s_pres += sensor.data.pressure
            s_hum += sensor.data.humidity

    m_temp = round(s_temp / s_cnt, 2)
    m_pres = round(s_pres / s_cnt, 2)
    m_hum = round(s_hum / s_cnt, 2)
    timestamp_ns = time.time_ns()
    timestamp_local = datetime.now(tz=None).isoformat()

    return dict(time=timestamp_ns, temp=m_temp, rh=m_hum, pressure=m_pres, local_time=timestamp_local, host=HOST)


def connect_to_broker():
    try:
        mqttc.connect(MQTT_HOST, MQTT_PORT, MQTT_KEEPALIVE_INTERVAL)
        return True
    except Exception as e:
        logging.critical("[%s] connection to MQTT broker [%s:%s] failed", e, MQTT_HOST, MQTT_PORT)
        exit(1)


init_logging()
init_mqtt()
init_sensor()

mqttc = mqtt.Client(MQTT_CLIENT_ID)
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_log = on_log
start_time = time.time()

if connect_to_broker():
    mqttc.loop_start()

while not mqttc.client_connected:
    logging.info("waiting for client to connect")
    time.sleep(1)

while True:
    payload = read_sensor()
    obj = mqttc.publish(MQTT_TOPIC, json.dumps(payload))

    if obj.rc != 0:
        logging.error("failed to publish [%s], mid: %s, reason: %s", payload, obj.mid, obj.rc)
    else:
        logging.debug("published message [%s] mid: %s", payload, obj.mid)

    time.sleep(60.0 - ((time.time() - start_time) % 60.0))
