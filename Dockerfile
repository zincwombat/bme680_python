FROM arm32v6/python:3-alpine

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN apk update && apk upgrade && apk add --no-cache git make build-base linux-headers gcc libc-dev cmake ninja autoconf automake libtool
RUN pip install --no-cache-dir -r requirements.txt

#COPY scripts/* scripts/

CMD [ "python", "./scripts/sensor.py" ]
